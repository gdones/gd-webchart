# WEB版微信聊天程序

#### 介绍
基于SpringBoot+Vue的Web版微信聊天程序

#### 软件架构
软件架构说明
1. 后端（WebChartServer）：SpringBoot+MyBatisPlus+JWT+Redis
2. 前端：Vue2.0+ElementUI

#### 安装教程

1.后端教程：
    1.1 检出WebChartServer项目后使用idea打开，记得打开idea时要配置maven为本机环境，mirror地址配置为阿里云仓库
    1.2 修改SpringBoot配置文件中：数据库数据源连接池的配置内容
```yaml

```
    1.3 启动WebChartServerApplication.java 启动类即可

#### 使用说明

1.  后端程序依赖mysql和mysql中的数据表，需要执行项目中的初始化sql即可，在后端项目的sql文件夹下有“初始化.sql”文件执行即可

