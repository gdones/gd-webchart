// 1. 导入axios组件
import axios from "axios";

// 导入el组件
import ElementUI from 'element-ui'

// 导入路由组件
import router from '@/router'

const service = axios.create({// get post delete put
	baseURL:'/api',  // 后端服务地址
	timeout:30000 // 请求超时时间
});

// http request 请求拦截器
service.interceptors.request.use(config => {
     
    // 需要所有的请求都带有token
    config.headers.token = window.localStorage.getItem("token");

    return config;
}, error => {
	// 对请求错误做些什么
	return Promise.reject(error);
  
});


// 响应拦截器
service.interceptors.response.use(response => {

    // 当后端返回数据或者后端请求错误，我们需要处理相关内容
    if(response.status==200){// 请求发送响应成功

        if(response.data.code=="-1"){// 认证失败
          ElementUI.Notification.error({
            title: '系统异常',
            message: response.data.msg+",请重新登录"
          });
          router.push("/");
        }else if(response.data.code=="200"){
          if(response.data.msg){
            ElementUI.Message.success(response.data.msg);
          }
         
        }else if(response.data.code=="500"){
          ElementUI.Message.error(response.data.msg);
        }
        
        return response.data;
      }else{
        ElementUI.Notification.error({
          title: '系统异常',
          message: error.response.data.msg+",请联系管理员"
        });
        return response.data;
      }

});

export default service;