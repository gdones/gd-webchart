import Vue from 'vue'
import Router from 'vue-router'

// 1. 导入页面组件
import Login from '@/components/login'; // 登录界面
import Main from '@/components/main'; // 主界面
import Test from '@/components/text'; // 主界面

Vue.use(Router)

export default new Router({
  routes: [  // 2. 完成路由配置
    {
      path: '/', // 设置路由的地址
      name: 'Login', // 设置路由组件的名称（唯一性）
      component: Login // 关联一个需要跳转到的目标组件位置
    },
    {
      path: '/main',
      name: 'Main',
      component: Main
    },
    {
      path: '/test',
      name: 'Test',
      component: Test
    }
  ]
})
