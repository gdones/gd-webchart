// 请求用户登录、注册、好友查询、好友删除、好友列表信息
import request from '@/util/request';

/**
 * 登录操作
 * @param {*} param 传递登录的参数信息
 */
export function doLogin(param){
    const data = {
        ...param
    }
    return request({
        url:"/user/login", // api 地址
        method:"POST", // 请求方法
        data: data  // 参数列表   --- post\put
    });
}

/**
 * 注册用户
 * @param {*} param 
 * @returns 
 */
export function add(param){
    const data = {
        ...param
    }
    return request({
        url:"/user/add", // api 地址
        method:"POST", // 请求方法
        data: data  // 参数列表   --- post\put
    });
}

/**
 * 修改用户信息
 * @param {*} param 
 * @returns 
 */
export function update(param){
    const data = {
        ...param
    }
    return request({
        url:"/user/update", // api 地址
        method:"PUT", // 请求方法
        data: data  // 参数列表   --- post\put
    });
}

/**
 * 删除用户
 * @param {*} param 
 * @returns 
 */
export function deletes(param){
    const data = {
        pkID:param
    }
    return request({
        url:"/user/deletes", // api 地址
        method:"DELETE", // 请求方法
        params: data  // 参数列表   --- GET\delete 
    });
}

/**
 * 查询好友添加
 * @param {*} param 
 * @returns 
 */
export function getFList(param){
    const data = {
        ...param
    }
    return request({
        url:"/userf/getFList", // api 地址
        method:"GET", // 请求方法
        params: data  // 参数列表   --- GET\delete 
    });
}

/**
 * 添加好友
 * @param {*} param 
 * @returns 
 */
export function addF(param){
    const data = {
        ...param
    }
    return request({
        url:"/userf/addF", // api 地址
        method:"POST", // 请求方法
        data: data  // 参数列表   --- GET\delete 
    });
}

/**
 * 获取好友列表
 * @param {*} param 
 * @returns 
 */
export function getF(param){
    const data = {
        userID:param
    }
    return request({
        url:"/userf/getF", // api 地址
        method:"GET", // 请求方法
        params: data  // 参数列表   --- GET\delete 
    });
}

/**
 * 获取好友的历史信息
 * @param {*} param 
 * @returns 
 */
export function getMsgList(param){
    const data = {
        ...param
    }
    return request({
        url:"/msg/getMsgList", // api 地址
        method:"GET", // 请求方法
        params: data  // 参数列表   --- GET\delete 
    });
}

/**
 * 保存历史记录
 * @param {*} param 
 * @returns 
 */
export function addMsg(param){
    const data = {
        msg:param
    }
    return request({
        url:"/msg/add", // api 地址
        method:"POST", // 请求方法
        params: data  // 参数列表   --- GET\delete 
    });
}

