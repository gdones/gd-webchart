package com.webchartserver.userf.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.webchartserver.core.dto.MyResult;
import com.webchartserver.user.dto.User;
import com.webchartserver.userf.dto.UserF;
import com.webchartserver.userf.service.IUserFService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 好友信息管理
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
@RestController
@RequestMapping("/userf")
@Slf4j
public class UserFController {

    @Resource(name = "userFServiceImpl")
    private IUserFService iUserFService;

    /**
     * 查询好友
     * @param userID 自身id（排除自己不能自己添加自己为好友）
     * @param nickName 查询条件
     * @return
     */
    @GetMapping("/getFList")
    public MyResult getFList(String userID,String nickName){
        log.debug("-- 查询好友");

        MyResult result = new MyResult();

        List<User> flist = iUserFService.getFList(userID,nickName);

        result.setMsg("查询成功！");
        result.setData(flist);

        return result;
    }


    /**
     * 添加好友
     * @return
     */
    @PostMapping("/addF")
    public MyResult addF(@RequestBody UserF user){
        log.debug("-- 添加好友");

        MyResult result = new MyResult();

        boolean save = iUserFService.save(user);

        if(save){
            result.setMsg("添加成功！");
        }else{
            result.setCode(500);
            result.setMsg("添加失败！");
        }

        return result;
    }

    /**
     * 获取当前用户的好友列表
     * @param userID
     * @return
     */
    @GetMapping("/getF")
    public MyResult getF(String userID){
        log.debug("-- 获取好友列表");

        MyResult result = new MyResult();

        QueryWrapper<UserF> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userID",userID);

        List<UserF> list = iUserFService.list(queryWrapper);

        result.setData(list);

        return result;
    }

}
