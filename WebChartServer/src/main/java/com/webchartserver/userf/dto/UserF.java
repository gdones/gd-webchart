package com.webchartserver.userf.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
@Data
@TableName("t_user_f")
public class UserF implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 记录主键
     */
    @TableId(value = "infoID", type = IdType.ASSIGN_UUID)
    private String infoID;

    /**
     * 记录插入者帐号
     */
    private String insertID;

    /**
     * 记录插入者IP
     */
    private String insertIP;

    /**
     * 记录更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 记录更新者帐号
     */
    private String updateID;

    /**
     * 记录更新者IP
     */
    private String updateIP;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 备注
     */
    private String comment;

    /**
     * 用户id
     */
    private String userID;

    /**
     * 好友用户id
     */
    private String fedUserID;

    /**
     * 好友昵称（用户名）
     */
    private String fedNickName;

    /**
     * 好友真实姓名
     */
    private String fedRealName;


}
