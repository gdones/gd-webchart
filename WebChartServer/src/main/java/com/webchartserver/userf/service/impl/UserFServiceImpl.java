package com.webchartserver.userf.service.impl;

import com.webchartserver.user.dto.User;
import com.webchartserver.userf.dto.UserF;
import com.webchartserver.userf.mapper.UserFMapper;
import com.webchartserver.userf.service.IUserFService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
@Service
public class UserFServiceImpl extends ServiceImpl<UserFMapper, UserF> implements IUserFService {

    @Override
    public List<User> getFList(String userID, String nickName) {

        UserFMapper userFMapper = getBaseMapper();

        List<User> list = userFMapper.getFList(userID,nickName);

        return list;
    }
}
