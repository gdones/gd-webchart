package com.webchartserver.userf.service;

import com.webchartserver.user.dto.User;
import com.webchartserver.userf.dto.UserF;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
public interface IUserFService extends IService<UserF> {

    List<User> getFList(String userID, String nickName);
}
