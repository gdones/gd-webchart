package com.webchartserver.userf.mapper;

import com.webchartserver.user.dto.User;
import com.webchartserver.userf.dto.UserF;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
@Mapper
public interface UserFMapper extends BaseMapper<UserF> {

    List<User> getFList(@Param("userID") String userID,@Param("nickName")  String nickName);
}
