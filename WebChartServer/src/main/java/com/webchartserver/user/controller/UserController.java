package com.webchartserver.user.controller;


import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.extra.pinyin.PinyinUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.webchartserver.core.config.JWTUtil;
import com.webchartserver.core.dto.MyResult;
import com.webchartserver.user.dto.User;
import com.webchartserver.user.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 用户信息管理
 * @author GaoJingBo
 * @since 2023-04-13
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    // 注入业务层实现
    @Resource(name = "userServiceImpl")
    private IUserService userService;

    // 注入配置文件中的参数值
    @Value("${webchart.login-err-count}")
    int errCount;

    /**
     * 获取用户通过用户id
     * @param userID
     * @return
     */
    @GetMapping("/getByID")
    public MyResult getUserByID(String userID){
        log.debug("-- 获取用户通过用户id");

        MyResult result = new MyResult();

        User byId = userService.getById(userID);

        result.setMsg("查询成功！");
        result.setData(byId);

        return result;
    }

    @PostMapping("/login")
    public MyResult login(@RequestBody User user){
        log.debug("-- 登录");

        MyResult result = new MyResult();

        // 1. 查询数据库中和用户名相同的数据
        QueryWrapper<User> queryWrapper = new QueryWrapper<>(); // from t_user
        // where nickName = ""
        queryWrapper.eq("nickName",user.getNickName());

        // 2. mp 查询   select * from t_user where nickName = "user.getNickName()"
        List<User> list = userService.list(queryWrapper);

        if(list.size()>0){ // 用户名存在

            User dbUserData = list.get(0);

            String pwdMD5 = dbUserData.getPwd();// 取出数据库中保存的密码-密文

            String pwd_md5Hex = DigestUtil.md5Hex(user.getPwd()); // 将前端的明文密码加密比较

            if(pwd_md5Hex.equals(pwdMD5)){ // 密码输入正确

                Map<String,Object> resMap = new HashMap<>();

                result.setCode(200);
                result.setMsg("登录成功！");

                // 要返回token令牌到前端
                String token = JWTUtil.getToken(user.getNickName());

                resMap.put("token",token);
                resMap.put("user",dbUserData);
                result.setData(resMap);

            }else{ // 密码输入错误
                result.setCode(500);

                // 已错误次数累加1
                dbUserData.setErrCount(dbUserData.getErrCount()+1);
                // 剩余几次可以输入密码的机会 = errCount - 已错误的次数
                int lessCount  = errCount - dbUserData.getErrCount();

                // 判断是否需要锁定
                if(lessCount<=0){ //需要锁定
                    dbUserData.setIsLock(1); // 设置账号锁定
                    // 返回错误信息给前端
                    result.setMsg("错误次数过多，账号已锁定！");
                }else{
                    result.setMsg("用户名或密码错误，还剩【"+lessCount+"】次机会！");
                }

                // 更新数据库
                userService.updateById(dbUserData);
            }

        }else{// 用户名不存在
            result.setCode(500); // 业务编码500描述业务不成功
            result.setMsg("用户名或密码错误！");
        }


        return result;

    }

    /**
     * 用户添加
     * @param user 要添加的用户信息
     * @return
     */
    @PostMapping("/add")
    public MyResult add(@RequestBody User user){
        log.debug("-- 注册用户");

        MyResult result = new MyResult();

        // 生成微信号
        String code = "WX"+ PinyinUtil.getFirstLetter(user.getNickName(),"")+new Date().getTime();
        user.setCode(code);
        // 密码加密
        String pwd_md5Hex = DigestUtil.md5Hex(user.getPwd());
        user.setPwd(pwd_md5Hex);

        boolean save = userService.save(user);

        result.setMsg("用户注册成功！");

        return result;
    }


    /**
     * 修改用户
     * @param user 要修改的用户信息（前端需要传递要修改的主键字段值）
     * @return
     */
    @PutMapping ("/update")
    public MyResult update(@RequestBody User user){
        log.debug("-- 修改用户");

        MyResult result = new MyResult();

        boolean save = userService.updateById(user);

        result.setMsg("用户修改成功！");

        return result;

    }

    /**
     * 删除用户
     * @param pkID 传递主键字段值
     * @return
     */
    @DeleteMapping ("/deletes")
    public MyResult deletes(String pkID){
        log.debug("-- 删除用户");

        MyResult result = new MyResult();

        boolean b = userService.removeById(pkID);

        result.setMsg("删除成功！");

        return result;
    }


}
