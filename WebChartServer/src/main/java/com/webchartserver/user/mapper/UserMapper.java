package com.webchartserver.user.mapper;

import com.webchartserver.user.dto.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-13
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
