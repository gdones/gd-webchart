package com.webchartserver.user.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-13
 */
@Getter
@Setter
@TableName("t_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(value = "userID", type = IdType.ASSIGN_UUID)
    private String userID;


    private String insertID;


    private String insertIP;


    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


    private String updateID;


    private String updateIP;


    @TableLogic
    private Integer isDelete;

    private String comment;

    private String code;

    private String realName;

    private String nickName;

    private Integer sex;

    private String tellNumber;

    private String pwd;

    private Integer isLock;

    private Integer errCount;

}
