package com.webchartserver.user.service;

import com.webchartserver.user.dto.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-13
 */
public interface IUserService extends IService<User> {

}
