package com.webchartserver.core.intercetes;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 模块名称：配置注册拦截器类
 * 模块类型：配置类
 * 编码人：高靖博
 * 创建时间：2023/4/14
 * 联系电话：18587388612
 */
@Configuration
public class InterceptConfig implements WebMvcConfigurer {

    //注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 注册自定义的登录拦截器
        InterceptorRegistration ic = registry.addInterceptor(new LoginInterceptor());
        // 设置要拦截的路径有哪些
        ic.addPathPatterns("/**"); // 所有请求服务端的路径地址都会被拦截

        // 设置拦截器白名单
        ic.excludePathPatterns(
                "/user/login/**", // 登录接口不需要拦截
                "/js/**", // 访问数据监控中心不需要拦截
                "/css/**",
                "/imgs/**",
                "/druid/**",
                "/v2/**",
                "/webjars/**",
                "/msg/**");

    }
}
