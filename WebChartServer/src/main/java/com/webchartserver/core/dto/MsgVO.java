package com.webchartserver.core.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 数据发送实体
 * 2023/4/17
 */
@Data
public class MsgVO implements Serializable {

    private String sendID;

    private String saveID;

    private String saveName;

    private String msg;

}
