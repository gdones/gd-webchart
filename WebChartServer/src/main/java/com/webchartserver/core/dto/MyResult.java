package com.webchartserver.core.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 模块名称：后端返回到前端的结果集类
 * 模块类型：
 * 编码人：高靖博
 * 创建时间：2023/4/13
 * 联系电话：18587388612
 */
@Data
public class MyResult implements Serializable {

    private int code = 200; // 状态码

    private String msg = "操作成功！"; // 结果描述

    private Object data; // 返回的数据包

}
