package com.webchartserver.core.mpganer;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;

/**
 * 模块名称：
 * 模块类型：
 * 编码人：高靖博
 * 创建时间：2023/4/13
 * 联系电话：18587388612
 */
public class MyBatisGer {

    // 数据库的连接 ！！！需要修改
    private final static String URL = "jdbc:mysql://localhost:3306/webchart?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=Asia/Shanghai";
    // 数据库用户名
    private final static String USER_NAME = "root";
    // 数据库密码
    private final static String PWD = "cxk666";
    // 每个类作者的名字
    private final static String AUTHOR = "GaoJingBo";

    // 生成的代码输出的目录
    private final static String OUT_PUT_DIR = "D://mybatis";

    // 修改根包路径
    private final static String PARENT_PACKAGE_NAME = "com.webchartserver";

    // 业务模块名
    private final static String PARENT_MODEL_NAME = "msg";

    // 要生成的业务表
    private final static String TABLE_NAME = "t_msg";

    // 如果表名有前缀可以去除前缀
    private final static String PREFIX = "t_";


    public static void main(String[] args) {

        FastAutoGenerator.create(URL, USER_NAME, PWD)
                .globalConfig(builder -> {
                    builder.author(AUTHOR) // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .dateType(DateType.ONLY_DATE)// 日期类型
                            .commentDate("yyyy-MM-dd") //公共默认日期格式
                            .outputDir(OUT_PUT_DIR); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent(PARENT_PACKAGE_NAME) //设置父路径根包
                            .moduleName(PARENT_MODEL_NAME) //设置模块名称
                            .entity("dto") //dto实体
                            .service("service") //业务接口
                            .serviceImpl("service.impl") //业务实现
                            .mapper("mapper") //mybatis-plus-mapper接口
                            .xml("mapper.xml") ////mybatis-plus-mapper接口映射xml
                            .controller("controller") //控制器
                            .other("other");
                })
                .strategyConfig(builder -> {
                    builder.controllerBuilder()
                            .enableHyphenStyle()
                            .enableRestStyle();
                    builder.mapperBuilder()
                            .enableMapperAnnotation();
                    builder.entityBuilder()
                            .enableLombok()
                            .logicDeleteColumnName("isDelete")// 逻辑删除表字段名
                            .logicDeletePropertyName("isDelete")// 逻辑删除实体属性名
                            // 添加填充规则
                            .addTableFills(new Column("insertTime", FieldFill.INSERT))
                            .addTableFills(new Column("updateTime", FieldFill.INSERT_UPDATE))
                            .idType(IdType.NONE);
                    builder.addInclude(TABLE_NAME) // 设置需要生成的表名
                            .enableSkipView()//跳过视图
                            .addTablePrefix(PREFIX); // 设置过滤表前缀

                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
