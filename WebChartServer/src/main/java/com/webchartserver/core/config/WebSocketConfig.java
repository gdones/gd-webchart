package com.webchartserver.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * 模块名称： 开启WebSocket服务
 * 模块类型：
 * 编码人：高靖博
 * 创建时间：2023/4/14
 * 联系电话：18587388612
 */
@Configuration
public class WebSocketConfig {

    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

}
