package com.webchartserver.core.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;

/**
 * 模块名称：JWT权限验证工具包
 * 模块类型：
 * 编码人：高靖博
 * 创建时间：2023/4/14
 * 联系电话：18587388612
 */
public class JWTUtil {

    private JWTUtil(){}

    // 过期时间，单位秒
    private static final long EXP_TIME = 60 * 30L;

    // 秘钥关键字
    private static final String SECRET = "webchart";

    /**
     * 生成token
     * @param userName 登录用户名
     * @return
     */
    // 生成token时，需要添加token信息
    // userName : 签名信息，保证token无法重复或者无法被篡改
    public static String getToken(String userName){

        // 创建Token构造器
        JWTCreator.Builder builder = JWT.create();

        //设置过期时间,设置什么时候过期：EXP_TIME计算一个过期的日子
        Date date = new Date(System.currentTimeMillis() + EXP_TIME * 1000);

        //创建token
        String sign = builder.withExpiresAt(date)
                .withClaim("userName", userName)
                .withClaim("claimDate", new Date().getTime())
                .sign(Algorithm.HMAC256(SECRET));

        return sign;
    }

    /**
     * 验证签名token
     * @param token
     * @return
     */
    public static boolean verify(String token){
        try{
            DecodedJWT verify = JWT.require(Algorithm.HMAC256(SECRET)).build().verify(token);
            System.out.println("------- [token认证通过] ---------");
            System.out.println("------- userName:"+verify.getClaim("userName")+" ---------");
            System.out.println("------- 过期时间:"+verify.getExpiresAt()+" ---------");
            return true;
        }catch (Exception e){
            return false;
        }

    }
}
