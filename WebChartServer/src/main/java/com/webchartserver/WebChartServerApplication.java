package com.webchartserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebChartServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebChartServerApplication.class, args);
    }

}
