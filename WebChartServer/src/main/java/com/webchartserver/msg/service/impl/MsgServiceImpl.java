package com.webchartserver.msg.service.impl;

import com.webchartserver.msg.dto.Msg;
import com.webchartserver.msg.mapper.MsgMapper;
import com.webchartserver.msg.service.IMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
@Service
public class MsgServiceImpl extends ServiceImpl<MsgMapper, Msg> implements IMsgService {

    @Override
    public List<Msg> getMsgList(String sendUserID, String saveUserID) {

        return getBaseMapper().getMsgList(sendUserID,saveUserID);
    }
}
