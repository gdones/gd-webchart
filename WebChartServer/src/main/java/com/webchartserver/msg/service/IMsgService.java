package com.webchartserver.msg.service;

import com.webchartserver.msg.dto.Msg;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
public interface IMsgService extends IService<Msg> {

    List<Msg> getMsgList( String sendUserID,String saveUserID);
}
