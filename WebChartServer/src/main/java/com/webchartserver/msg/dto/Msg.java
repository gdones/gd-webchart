package com.webchartserver.msg.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
@Getter
@Setter
@TableName("t_msg")
public class Msg implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 记录id
     */
    @TableId(value = "infoID", type = IdType.ASSIGN_UUID)
    private String infoID;

    /**
     * 记录插入者帐号
     */
    private String insertID;

    /**
     * 记录插入者IP
     */
    private String insertIP;

    /**
     * 记录更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 记录更新者帐号
     */
    private String updateID;

    /**
     * 记录更新者IP
     */
    private String updateIP;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 备注
     */
    private String comment;

    /**
     * 发送人的用户id
     */
    private String sendUserID;

    /**
     * 发送人的用户名
     */
    private String sendNickName;

    /**
     * 接收人的用户id
     */
    private String saveUserID;

    /**
     * 接收人的用户名称
     */
    private String saveNickName;

    /**
     * 消息内容
     */
    private String msg;

    /**
     * 接收消息时间
     */
    private Date saveAndSendTime;


}
