package com.webchartserver.msg.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.webchartserver.core.dto.MsgVO;
import com.webchartserver.core.dto.MyResult;
import com.webchartserver.core.utils.FilterWordsUtil;
import com.webchartserver.msg.dto.Msg;
import com.webchartserver.msg.service.IMsgService;
import com.webchartserver.userf.dto.UserF;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 *
 * 聊天信息管理
 * @author GaoJingBo
 * @since 2023-04-17
 */
@RestController
@RequestMapping("/msg")
@Slf4j
public class MsgController {

    @Resource(name = "msgServiceImpl")
    private IMsgService msgService;


    /**
     * 查询某个好友的历史记录信息( 谁给谁发送的信息 )
     * @param sendUserID 发送人的id
     * @param saveUserID 接收人的id
     * @return
     */
    @GetMapping("/getMsgList")
    public MyResult getMsgList(String sendUserID,String saveUserID){
        log.debug("-- 获取某个好友的历史信息");

        MyResult result = new MyResult();

        List<Msg> list = msgService.getMsgList(sendUserID,saveUserID);

        result.setData(list);

        return result;
    }

    /**
     *
     * 保存历史信息
     * @param msg
     * @return
     */
    @PostMapping("/add")
    public MyResult addMsg(String msg){
        log.debug("-- 保存历史信息");

        MyResult result = new MyResult();

        // 客户端发送信息的格式为json
        String[] arr = msg.split("@");

        //记录历史信息
        Msg msgData = new Msg();
        msgData.setSendUserID(arr[0]);
        msgData.setSendNickName(arr[1]);
        msgData.setSaveUserID(arr[2]);
        msgData.setSaveNickName(arr[3]);
        msgData.setSaveAndSendTime(new Date());
        msgData.setMsg(FilterWordsUtil.changeWord(arr[4], '*'));

        msgService.save(msgData);

        return result;
    }


    @GetMapping("/testMgFilter")
    public MyResult test(String msg){

        String s = FilterWordsUtil.changeWord(msg, '*');

        MyResult result = new MyResult();

        result.setData(s);

        return result;

    }


}
