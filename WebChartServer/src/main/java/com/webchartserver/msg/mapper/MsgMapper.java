package com.webchartserver.msg.mapper;

import com.webchartserver.msg.dto.Msg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author GaoJingBo
 * @since 2023-04-17
 */
@Mapper
public interface MsgMapper extends BaseMapper<Msg> {

    List<Msg> getMsgList(@Param("sendUserID")String sendUserID,@Param("saveUserID") String saveUserID);
}
